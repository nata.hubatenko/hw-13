let img = ["img/1.jpg", "img/2.jpg", "img/3.jpg", "img/4.png"]

let index = 0;

let nInterval;

function starChange() {

	if (!nInterval) {
		nInterval = setInterval(changeImg, 3000);
	}
}

starChange();

function changeImg() {

	if (index === img.length) {
		index = 0;
	}
	document.getElementById("image").src = img[index];
	console.log(img[index]);
	index++;

}

function stopChange() {
	clearInterval(nInterval);
	// release our nInterval from the variable
	nInterval = null;
}

document.getElementById("start").addEventListener("click", starChange);
document.getElementById("stop").addEventListener("click", stopChange);


























